package ru.tsc.kyurinova.tm.service.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.api.service.dto.ISessionDTOService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIndexException;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionDTOService extends AbstractDTOService implements ISessionDTOService {

    @NotNull
    @Autowired
    public ISessionDTORepository sessionRepository;

    @NotNull
    @Autowired
    public IUserDTORepository userRepository;

    @Autowired
    private IPropertyService propertyService;

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public SessionDTO open(@NotNull final String login, @NotNull final String password) {
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    @NotNull
    @Transactional
    public SessionDTO sign(@NotNull final SessionDTO session) {
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSecret();
        final int cycle = propertyService.getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    @Transactional
    public void close(@Nullable SessionDTO session) {
        if (session == null) return;
        sessionRepository.removeById(session.getId());
    }

    @Override
    public boolean exists(@NotNull final String sessionId) {
        @NotNull final SessionDTO session1 = sessionRepository.findById(sessionId);
        if (session1 != null) return true;
        else return false;
    }

    @Override
    public void validate(@NotNull final SessionDTO session) {
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final SessionDTO sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!exists(session.getId())) throw new AccessDeniedException();
    }

    @SneakyThrows
    @Override
    public void validate(@NotNull SessionDTO session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final UserDTO user = userRepository.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @Transactional
    public void remove(@Nullable final SessionDTO entity) {
        if (entity == null) throw new EntityNotFoundException();
        sessionRepository.remove(entity);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        sessionRepository.clear();
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return sessionRepository.findById(id);
    }

    @NotNull
    @Override
    public SessionDTO findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return sessionRepository.findByIndex(index);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        sessionRepository.removeById(id);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        sessionRepository.removeByIndex(index);
    }

    @Override
    public int getSize() {
        return sessionRepository.getSize();
    }

}
