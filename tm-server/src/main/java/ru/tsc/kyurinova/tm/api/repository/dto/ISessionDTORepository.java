package ru.tsc.kyurinova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDTORepository extends IDTORepository {

    void add(
            @NotNull final SessionDTO session
    );

    @Nullable
    public SessionDTO findById(
            @NotNull final String id
    );

    void remove(
            @NotNull final SessionDTO session
    );

    @Nullable
    List<SessionDTO> findAll(
    );

    void clear(
    );

    @Nullable
    SessionDTO findByIndex(
            @NotNull final Integer index
    );

    void removeByIndex(
            @NotNull final Integer index
    );

    void removeById(
            @NotNull final String id
    );

    int getSize(
    );

}
