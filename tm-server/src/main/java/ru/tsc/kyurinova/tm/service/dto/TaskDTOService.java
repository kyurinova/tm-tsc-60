package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kyurinova.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class TaskDTOService extends AbstractDTOService implements ITaskDTOService {

    @NotNull
    @Autowired
    public ITaskDTORepository taskRepository;


    @Override
    @Transactional
    public void addAll(@NotNull final List<TaskDTO> tasks) {
        for (TaskDTO task : tasks) {
            taskRepository.add(task.getUserId(), task);
        }
    }


    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO(userId, name, "");
        taskRepository.add(userId, task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO(userId, name, description);
        taskRepository.add(userId, task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO taskDTO = findByName(userId, name);
        if (taskDTO != null) {
            taskRepository.removeByName(userId, name);
        }
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO taskDTO = findById(userId, id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskRepository.update(userId, taskDTO);
    }

    @Override
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        TaskDTO taskDTO = findByIndex(userId, index);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskRepository.update(userId, taskDTO);
    }

    @Override
    @Transactional
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.startById(userId, id);
    }

    @Override
    @Transactional
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        taskRepository.startByIndex(userId, index);
    }

    @Override
    @Transactional
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.startByName(userId, name);
    }

    @Override
    @Transactional
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.finishById(userId, id);
    }

    @Override
    @Transactional
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        taskRepository.finishByIndex(userId, index);
    }

    @Override
    @Transactional
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.finishByName(userId, name);
    }

    @Override
    @Transactional
    public void changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        taskRepository.changeStatusById(userId, id, status);
    }

    @Override
    @Transactional
    public void changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        taskRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    @Transactional
    public void changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        taskRepository.changeStatusByName(userId, name, status);
    }

    @Nullable
    @Override
    public TaskDTO findByProjectAndTaskId(@Nullable final String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        return taskRepository.findByProjectAndTaskId(userId, projectId, taskId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EntityNotFoundException();
        taskRepository.remove(userId, task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAllUserId(userId);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.clearUserId(userId);
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @NotNull
    @Override
    public TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        TaskDTO taskDTO = findById(userId, id);
        if (taskDTO != null) {
            taskRepository.removeById(userId, id);
        }
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        TaskDTO taskDTO = findByIndex(userId, index);
        if (taskDTO != null) {
            @NotNull final String id = taskRepository.findByIndex(userId, index).getId();
            taskRepository.removeById(userId, id);
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        @Nullable final TaskDTO task = taskRepository.findById(userId, id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final TaskDTO task = taskRepository.findByIndex(userId, index);
        return task != null;
    }

}
