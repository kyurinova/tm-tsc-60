package ru.tsc.kyurinova.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.model.IRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@NoArgsConstructor
@AllArgsConstructor
@Scope("prototype")
public abstract class AbstractRepository implements IRepository {

    @NotNull
    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

}

