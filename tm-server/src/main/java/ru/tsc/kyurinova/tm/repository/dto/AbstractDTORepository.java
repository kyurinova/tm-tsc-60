package ru.tsc.kyurinova.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.dto.IDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Getter
@Repository
@NoArgsConstructor
@AllArgsConstructor
@Scope("prototype")
public abstract class AbstractDTORepository implements IDTORepository {

    @NotNull
    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

}

