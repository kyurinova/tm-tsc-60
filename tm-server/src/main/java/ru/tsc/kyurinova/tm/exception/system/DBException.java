package ru.tsc.kyurinova.tm.exception.system;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class DBException extends AbstractException {

    public DBException() {
        super("Database operation exception");
    }

}
